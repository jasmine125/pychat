import socket
import threading


IP = '127.0.0.1'    # IP
PORT = 5000         # PORT


def start_server_echo(conn: socket, address: tuple) -> None:
    """
    클라이언트 메시지를 수신하고 메시지를 다시 클라이언트에 보낸다.

    @param conn: socket instance
    @param address: tuple(IP, pid)
    """
    print("Connection from: " + str(address))
    conn.send(f'Hello {str(address)}'.encode())
    while True:
        # 메시지 수신. 1024 bytes 이상은 수신 못한다.
        data = conn.recv(1024).decode()
        if not data:
            break
        print(f"from {str(address)}: {str(data)}")
        conn.send(data.encode())  # 클라이언트로 메시지 발송
    conn.close()  # 연결 해제

def server_program():
    """
    서버 소켓 실행    
    """
    server_socket = socket.socket()
    server_socket.bind((IP, PORT))

    while True:
        server_socket.listen(2)
        conn, address = server_socket.accept()  # 신규 connection을 받는다.
        # 접속된 connection을 쓰레드로 돌리고 다음 connection을 대기한다.
        threading.Thread(target=start_server_echo, args=(conn, address)).start()


if __name__ == '__main__':
    server_program()