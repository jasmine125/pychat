import os
import socket
import threading
import time


IP = '127.0.0.1'    # IP
PORT = 5000         # PORT


def receiving(conn: socket) -> None:
    """
    서버로부터 메시지를 수신하여 출력한다.
    서버와 통신이 끊어질때 sys.exit(0)를 해봤지만 이 thread만 종료될뿐 
    main thread는 종료되지 않아서 강제로 프로세스를 kill 시킨다.

    @param conn: socket instance
    """
    try:
        while True:
            data = conn.recv(1024).decode()  # receive response
            print(f'Received from server: {data}\n')  # show in terminal
    except:
        pass
    finally:
        print('Disconnected!')
        conn.close()
        pid = os.getpid()
        os.kill(pid, 2)


def sending(conn: socket) -> None:
    """
    메시지를 서버로 발송한다.
    서버와 통신이 끊어질때 sys.exit(0)를 해봤지만 이 thread만 종료될뿐 
    main thread는 종료되지 않아서 강제로 프로세스를 kill 시킨다.

    @param conn: socket instance
    """
    try:
        while True:
            message = input('-> ')
            conn.send(message.encode())
    except:
        pass
    finally:
        print('Disconnected!')
        conn.close()
        pid = os.getpid()
        os.kill(pid, 2)

def client_program():
    """
    서버에 접속하여 보내기/ 받기를 별도의 쓰레드로 만들어 실행한다.
    threading.Thread()에서 args는 반드시 iterable만 가능하기 때문에
    args를 (client_socket, )으로 넘긴다.
    """
    while True:
        try:
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # 서버 소켓에 접속
            client_socket.connect((IP, PORT))
            break
        except ConnectionRefusedError:
            print('Connection Failed, Retrying..')
            time.sleep(1)
    threading.Thread(target=receiving, args=(client_socket,)).start()
    threading.Thread(target=sending, args=(client_socket,)).start()


if __name__ == '__main__':
    client_program()